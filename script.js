// task 1

const link = document.createElement("a");

link.textContent = "Learn More";

link.setAttribute("href", "#");

const footer = document.querySelector("footer");

const text = footer.querySelector("p");

footer.insertBefore(link, text);



// task 2

const select = document.createElement("select");

select.id = "rating";

const main = document.querySelector("main");

const features = document.getElementById("Features");

main.insertBefore(select, features);

const options = [
  { value: "4", text: "4 Stars" },
  { value: "3", text: "3 Stars" },
  { value: "2", text: "2 Stars" },
  { value: "1", text: "1 Star" },
];

options.forEach(function (option) {
  const option = document.createElement("option");
  option.value = option.value;
  option.textContent = option.text;
  select.appendChild(option);
});
